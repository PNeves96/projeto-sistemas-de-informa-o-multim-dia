<?php

	include 'functions.php';

	$pdo = create_database_connection();

	$action= $_GET['action'];

	$user=$_GET['user'];

	$my_id=$_SESSION["user_id"];

	if($action=='enviar'){
		$enviar = $pdo->prepare("INSERT INTO pedidosamigo VALUES(0,:id,:user)");
		$enviar->bindParam(':id',$_SESSION['user_id']);
		$enviar->bindParam(':user',$_GET['user']);
		$enviar->execute();
	}

	if($action=='cancelar'){
		$cancelar = $pdo->prepare("DELETE FROM pedidosamigo WHERE de=:id AND para=:user");
		$cancelar->bindParam(':id',$_SESSION['user_id']);
		$cancelar->bindParam(':user',$_GET['user']);
		$cancelar->execute();
	}

	if($action=='aceitar'){
		$apagarpedido = $pdo->prepare("DELETE FROM pedidosamigo WHERE de=:user AND para=:id");
		$apagarpedido->bindParam(':id',$_SESSION['user_id']);
		$apagarpedido->bindParam(':user',$_GET['user']);
		$apagarpedido->execute();
		$aceitar = $pdo->prepare("INSERT INTO amigos VALUES(0,:user,:id)");
		$aceitar->bindParam(':id',$_SESSION['user_id']);
		$aceitar->bindParam(':user',$_GET['user']);
		$aceitar->execute();
	}

	if($action=='remover'){
		$remover = $pdo->prepare("DELETE FROM amigos WHERE (amigo1=:user AND amigo2=:id) OR (amigo1=:id AND amigo2=:user)");
		$remover->bindParam(':id',$_SESSION['user_id']);
		$remover->bindParam(':user',$_GET['user']);
		$remover->execute();
	}

	if($action=='ignorar'){
		$ignorar = $pdo->prepare("DELETE FROM pedidosamigo WHERE de=:user AND para=:id");
		$ignorar->bindParam(':id',$_SESSION['user_id']);
		$ignorar->bindParam(':user',$_GET['user']);
		$ignorar->execute();
	}
	header('location:perfil.php?user='.$user);