<html>
<head>
<title>Amigos</title>
<link rel='stylesheet' href='style.css'/>
<head>
<body>
<?php 
include 'functions.php';
include 'header.php';
$pdo = create_database_connection();
?>
<div class='container'>
	<h3>Amigos:</h3>
	<?php
		$amigos = $pdo->prepare("SELECT amigo1, amigo2 FROM amigos WHERE amigo1=:id OR amigo2=:id");
		$amigos->bindParam(':id',$_SESSION['user_id']);
		$amigos->execute();
		$users=get_all_users($pdo);
		foreach($amigos as $amigo){
			$amigo1=$amigo['amigo1'];
			$amigo2=$amigo['amigo2'];
			if($amigo1==$my_id){
				$user=$amigo2;
			}else{
				$user=$amigo1;
			}
			foreach($users as $userss){
				if($userss['id']==$user){
					$nome=$userss['nome'];
					echo "<a href='perfil.php?user=$user' class='box' style='display:inherit'>$nome</a>";
				}
			}
		}
	?>
</div>
</body>
</html>