<html>
<head>
<title>Login</title>
<link rel='stylesheet' href='style.css'/>
</head>
<body>
<?php 
include 'functions.php';
include 'header.php';
$pdo = create_database_connection();

if(!loggedin()){
?>
<div class='container'>
	<h1>Login</h1>
	<form method='post'>
	<?php
	if (isset($_POST['submit'])) {
		$username=$_POST['username'];
		$password=$_POST['password'];
		if(empty($username) or empty($password)){
			$message ="Por favor preencha todos os campos";
		}else{
			$users=get_all_users($pdo);

			foreach($users as $user){
				if($user["nome"]==$username && $user["password"]==$password){
					if($user["tipo"]=="user"){
						echo "<h1>user</h1>";
					}else if($user["tipo"]=="admin"){
						echo "<h1>admin</h1>";
					}
					$message="";
					$user_id=$user["id"];
					$_SESSION['user_id']=$user_id;
					header('location: index.php');
				}else{
					$message="Username ou Password incorreta";
				}
			}
		}
		if($message!=''){
			echo"<div class='box'>$message</div>";
		}
	}
	?>
	Username:<br/>
	<input type='text' name='username' autocomplete="off" />
	<br/><br/>
	Password:<br/>
	<input type='password' name='password'/>
	<br/><br/>
	<input type='submit' name='submit' value='Login'>
	</form>

</div>
<?php
}else{
	header('location: index.php');
}
?>
</body>
</html>