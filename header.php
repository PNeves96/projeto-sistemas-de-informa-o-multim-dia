<div id='title_bar'>
<ul>
<li><a href='index.php'>Home</a></li>
	<?php
	if(loggedin()){
		?>
		<li><a href='perfil.php'>Perfil</a></li>
		<li><a href='pedidos.php'>Pedidos</a></li>
		<li><a href='amigos.php'>Amigos</a></li>
		<li><a href='membros.php'>Membros</a></li>
		<li><a href='editar.php'>Editar Perfil</a></li>
		<?php
		$my_id=$_SESSION['user_id'];
		$pdo = create_database_connection();
		$users=get_all_users($pdo);

		foreach($users as $user){
			$user_id=$user["id"];
			if($user_id==$my_id){
				if($user['tipo']=='admin'){
					?>
						<li><a href='inserir.php'>Inserir Utilizador</a></li>
						<li><a href='apagar.php'>Apagar Utilizador</a></li>
					<?php
				}
			}
		}
		?>
		<div class="topright">
		<?php
		foreach($users as $user){
			$userid=$user["id"];
			if($userid==$my_id){
				$nome=$user['nome'];
				echo "<h3>Logged in as: $nome</h3>";
			}
		}
		?>
		<li><a href='logout.php'>Logout</a></li>
		</div>
		<?php
	}else{
		?>
		<li><a href='login.php'>Log in</a></li>
		<li><a href='guest.php'>Entrar como guest</a></li>
		<?php
	}
	?>
	<div class='clear'></div>
</ul>
</div>