<?php
	
session_start();

function loggedin(){
	if(isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])){
		return true;
	}else{	
		return false;
	}
}

function get_all_users($pdo){
	$statement = $pdo->prepare("SELECT * FROM users");
	$statement->execute();
	$all_users = $statement->fetchAll();
	return $all_users;
}

function create_database_connection(){
	try {
	    $pdo = new PDO('mysql:host=localhost;dbname=ulhtbook', 'root', '');
	    return $pdo;
	} catch (PDOException $e) {
	    print $e->getMessage();
	    return false;
	}
}