<html>
<head>
<title>Pedidos de amizade</title>
<link rel='stylesheet' href='style.css'/>
<head>
<body>
<?php 
include 'functions.php';
include 'header.php';
$pdo = create_database_connection();
?>
<div class='container'>
	<h3>Pedidos:</h3>
	<?php
		$pedidos = $pdo->prepare("SELECT de FROM pedidosamigo WHERE para=:id");
		$pedidos->bindParam(':id',$_SESSION['user_id']);
		$pedidos->execute();
		$users=get_all_users($pdo);
		foreach($pedidos as $pedido){
			$de=$pedido['de'];
			foreach($users as $user){
				if($user['id']==$de){
					$nome_de=$user['nome'];
					echo "<a href='perfil.php?user=$de' class='box' style='display:block'>$nome_de</a>";
				}
			}
		}
	?>
</div>
</body>
</html>