# README #

Trabalho realizado por:

* Luís Neves nº21500685
* Pedro Alves nº21403598
* Pedro Neves nº21402667

Convenções utilizadas:

* pdo-utilizador para a ligação às base de dados


Todos os ficheiros:

* actions.php-onde estão definidas as ações que podem acontecer sobre o perfil de um membro e os pedidos de amizade que recebe e/ou envia
* amigos.php-serve para qualquer membro visualizar os seus amigos
* apagar.php-serve para o admin apagar um membro
* editar.php-serve para editar um membro, podendo se for um user apenas editar o seu perfil ou se for um admin pode editar qualquer perfil
* functions.php-onde estão definidas as funções
* guest.php-serve para um guest ver todos os membros sem fazer login
* guest2.php-serve para um guest ver alguns dados de cada membro
* header.php-serve para criar o header da página
* index.php-pagina inicial do programa
* inserir.php-serve para o admin inserir um novo membro
* login.php-serve para se fazer login na conta de um membro
* logout.php-serve para se fazer logout da conta a que se fez login
* membros.php-serve para ver todos os membros
* pedidos.php-serve para ver todos os pedidos
* perfil.php-serve para ver o perfil de cada membro
* style.css-serve para definir o estilo da pagina

Base de dados:

* Users: inclui todos os utilizadores que podem fazer login
* pedidosamigo: inclui os pedidos de amigo que estão pendentes
* amigos: inclui as relações entre dois amigos, indicando se estes são amigos